import { FC } from 'react';
import { useSelector } from 'react-redux';
import { CoinData, CoinDescription } from '../interfaces';
import { StateType } from '../store/store';
import { Area, AreaChart } from 'recharts';

export const ShowCoinData: FC<CoinDescription> = (coin) => {
  const {
    normalizedValues,
    currentValue,
    priceDiff,
    percentageDiff,
    isPositiveDiff
  }: CoinData | undefined = useSelector<StateType, CoinData>(
    (state) => state.values.coins[coin.value]
  );

  return normalizedValues.length > 0 ? <div className='Coin-data'>
    <div data-testid='Description' className='Coin-description'>
      <div className='Name'>{coin.name}</div>
      <div className='Abbr'>{coin.abbr.toUpperCase()}</div>
    </div>
    <AreaChart data-testid='Chart' width={200} height={80} data={normalizedValues} className='Coin-graph'>
      <Area type="monotone" dataKey="value" stroke="#73cddd" fillOpacity={1} fill="#D8ECF2" />
    </AreaChart>
    <div data-testid='Prices' className='Prices'>
      <div>{currentValue} €</div>
      <div className={isPositiveDiff ? 'Price' : 'Negative-price'}>
        { isPositiveDiff ? '': '-' }€ {priceDiff} ({percentageDiff} %)
      </div>
    </div>
  </div> : null
}
