import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CoinDescription } from '../interfaces';
import { setCoinsForView } from '../store/coin-slice';
import { StateType } from '../store/store';

export const Search: FC = () => {
  const coinDescriptions: CoinDescription[] = useSelector<StateType, CoinDescription[]>(
    (state) => state.coins.all
  );
  const dispatch = useDispatch()

  const [input, setInput] = useState('')

  const onChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const input: string = event.target.value
    setInput(input)

    if (input.length === 0) {
      dispatch(setCoinsForView({ coins: coinDescriptions }))
    } else {
      const filtered: CoinDescription[] = coinDescriptions.filter(
        (suggestion: CoinDescription) =>
          suggestion.description.toLowerCase().includes(input.toLowerCase()))

      dispatch(setCoinsForView({ coins: filtered }))
    }
  }

  const onKeydown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    if (event.key === 'Enter') {
      event.preventDefault()
    }
  }

  return <form className='Search'>
    <input
      className='SearchInput'
      placeholder={'Search for coins'}
      value={input}
      autoFocus={false}
      onKeyDown={onKeydown}
      onChange={onChange}
    />
  </form>
}