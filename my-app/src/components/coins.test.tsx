import { render, screen } from '@testing-library/react';
import { Provider } from 'react-redux';
import { setCoinsForView } from '../store/coin-slice';
import store from '../store/store';
import { Coins } from './coins';

jest.mock(
  './show-coin-data',() => ({
    ShowCoinData: () => <div data-testid={'ShowCoinData'} />,
  }));

describe('Coins', () => {
  const componentRender = (): void => {
    render(<Coins />, {
      wrapper:
        ({children}) =>
          <Provider store={store}>{children}</Provider>
    })
  }

  it('should show correct number of ShowCoinData components', async () => {
    store.dispatch(setCoinsForView({coins: [
      {name: 'Bitcoin', abbr: 'BTC', value: 'btceur', description: 'Bitcoin BTC'},
      {name: 'Litecoin', abbr: 'LTC', value: 'ltceur', description: 'LiteCoin LTC'},
      {name: 'Etherium', abbr: 'ETH', value: 'etheur', description: 'Etherium ETH'},
    ]}))

    componentRender()

    expect(screen.getAllByTestId('ShowCoinData').length).toEqual(3)
  })

  it('should not show any component if no coin data should be shown', () => {
    store.dispatch(setCoinsForView({coins: []}))

    componentRender()

    expect(screen.queryByTestId('ShowCoinData')).toBeNull()
  })
})