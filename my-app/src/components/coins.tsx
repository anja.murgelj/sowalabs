import React, { FC } from 'react';
import { useSelector } from 'react-redux';
import { CoinDescription } from '../interfaces';
import { ShowCoinData } from './show-coin-data';
import { StateType } from '../store/store';

export const Coins: FC = () => {
  const showCoins: CoinDescription[] = useSelector<StateType, CoinDescription[]>(
    (state) => state.coins.viewCoins
  );

  return (
    <div className='Coins'>
      {showCoins.length > 0 ?
        showCoins.map(coin =>
          <ShowCoinData key={coin.abbr} {...coin} />
        ) : <div> Nothing to show. Please search for different coin. </div>
      }
    </div>
  )
}