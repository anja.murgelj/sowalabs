import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { CoinState, DataState } from '../interfaces';
import dataReducer from './data-slice';
import coinReducer from './coin-slice';

export interface StateType {
  values: DataState,
  coins: CoinState,
}

const reducers = combineReducers({
  values: dataReducer,
  coins: coinReducer,
})

const store = createStore(
  reducers,
  composeWithDevTools(
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    applyMiddleware(thunk),
));

export default store
