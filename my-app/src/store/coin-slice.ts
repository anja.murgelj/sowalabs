import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CoinDescription, CoinState } from '../interfaces';

const initialState: CoinState = {
  all: [],
  viewCoins: []
}

const  coinSlice = createSlice({
  name: 'coin',
  initialState,
  reducers: {
    initCoins(state: CoinState, action: PayloadAction<{ coins: CoinDescription[] }>) {
      state.all = action.payload.coins
    },
    setCoinsForView(state: CoinState, action: PayloadAction<{ coins: CoinDescription[] }>) {
      state.viewCoins = action.payload.coins
    }
  }
})

export const { initCoins, setCoinsForView } = coinSlice.actions

export default coinSlice.reducer