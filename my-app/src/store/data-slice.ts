import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { normalizeCloseValues } from '../helpers';
import { ApiParams, CoinData, DataState, OhlcType, TimeInterval } from '../interfaces';

const initCoin: CoinData = { normalizedValues: [], currentValue: '0', percentageDiff: '0', priceDiff: '0', isPositiveDiff: true }

const initialState: DataState = {
  coins: {
    'btceur': initCoin,
    'ltceur': initCoin,
    'etheur': initCoin,
    'unieur': initCoin,
    'xrpeur': initCoin,
    'algoeur': initCoin,
    'linkeur': initCoin,
    'adaeur': initCoin,
    'grteur': initCoin,
  }
}

const getParamsForTimeInterval = (timeInterval: TimeInterval): ApiParams => {
  switch (timeInterval) {
    case TimeInterval.ThreeHours:
      return {
        step: 60,
        limit: 180
      }
    case TimeInterval.Day:
      return {
        step: 900,
        limit: 240
    }
    case TimeInterval.Week:
      return {
        step: 1800,
        limit: 840
      }
    case TimeInterval.ThreeMonths:
      return {
        step: 14400,
        limit: 540
      }
    case TimeInterval.Year:
      return {
        step: 43200,
        limit: 731
      }
  }
}

export const fetchCoinValues = createAsyncThunk(
  'data/fetchCoinValues',
  async (
    {coin, timeInterval}: {coin: string, timeInterval: TimeInterval}, thunkAPI
  ): Promise<{ ohlc: OhlcType[]}> => {

    const url: string = 'https://www.bitstamp.net/api/v2/ohlc/'.concat(coin)
    const response = await axios.get(url, {params: getParamsForTimeInterval(timeInterval)})

    return response.data.data
  })

const dataSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(fetchCoinValues.fulfilled, (state, action) => {
      const lastIndex: number = action.payload.ohlc.length - 1

      const startPrice: number = parseFloat(action.payload.ohlc[0].close)
      const endPrice: number = parseFloat(action.payload.ohlc[lastIndex].close)
      const priceDiff: number = endPrice - startPrice

      state.coins[action.meta.arg.coin].normalizedValues = normalizeCloseValues(action.payload.ohlc)
      state.coins[action.meta.arg.coin].isPositiveDiff = priceDiff > 0
      state.coins[action.meta.arg.coin].currentValue = endPrice.toFixed(2)
      state.coins[action.meta.arg.coin].priceDiff = Math.abs(priceDiff).toFixed(2)
      state.coins[action.meta.arg.coin].percentageDiff = (((endPrice / startPrice) - 1) * 100).toFixed(2)
    })
    builder.addCase(fetchCoinValues.rejected, (state, action) => {
      // TODO for the future :)
      console.log('handle error')
    })
  }
})

export default dataSlice.reducer