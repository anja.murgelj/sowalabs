
export type CoinData = {
  normalizedValues: { value: number }[],
  currentValue: string,
  percentageDiff: string,
  priceDiff: string,
  isPositiveDiff: boolean
}

export type DataState = {
  coins: {
    [key: string]: CoinData,
  },
}

export type OhlcType = {
  open: string,
  close: string,
  high: string,
  timestamp: string,
  volume: string,
  low: string
}

export type CoinDescription = {
  name: string,
  abbr: string,
  value: string,
  description: string
}

export type CoinState = {
  all: CoinDescription[],
  viewCoins: CoinDescription[]
}

export enum TimeInterval {
  ThreeHours,
  Day,
  Week,
  ThreeMonths,
  Year,
}

export type ApiParams = {
  start?: number,
  step: number,
  limit: number,
}
