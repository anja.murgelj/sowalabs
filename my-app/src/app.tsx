import React, { FC, useState } from 'react';
import './app.sass';
import { Coins } from './components/coins';
import { Search } from './components/search';
import { TimeInterval } from './interfaces';
import { useInitialize } from './use-initialize';

const App: FC = () => {
  const [timeInterval, setTimeInterval] = useState(TimeInterval.ThreeHours)

  useInitialize(timeInterval)

  const getBtnClass = (interval: TimeInterval): string => interval === timeInterval ? 'Btn-highlighted' : ''

  return (
    <div className='App'>
      <div className='Header'>
        <Search />
        <div className='Filter'>
          <div
            className={getBtnClass(TimeInterval.ThreeHours)}
            onClick={() => setTimeInterval(TimeInterval.ThreeHours)}
          >
            3H
          </div>
          <div
            className={getBtnClass(TimeInterval.Day)}
            onClick={() => setTimeInterval(TimeInterval.Day)}
          >
            24H
          </div>
          <div
            className={getBtnClass(TimeInterval.Week)}
            onClick={() => setTimeInterval(TimeInterval.Week)}
          >
            7D
          </div>
          <div
            className={getBtnClass(TimeInterval.ThreeMonths)}
            onClick={() => setTimeInterval(TimeInterval.ThreeMonths)}
          >
            3M
          </div>
          <div
            className={getBtnClass(TimeInterval.Year)}
            onClick={() => setTimeInterval(TimeInterval.Year)}
          >
            1Y
          </div>
        </div>
      </div>
      <Coins />
    </div>
  )
}

export default App;
