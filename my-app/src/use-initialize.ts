import { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import { CoinDescription, TimeInterval } from './interfaces';
import { initCoins, setCoinsForView } from './store/coin-slice';
import { fetchCoinValues } from './store/data-slice';
import { coins } from './helpers';
import store from './store/store';

export const useInitialize = (timeInterval: TimeInterval): void => {
  let interval = useRef<number>(0)
  const dispatch = useDispatch()

  const fetchAllData = (): void => {
    coins.forEach((coin: CoinDescription) =>
      dispatch(fetchCoinValues({coin: coin.value, timeInterval: timeInterval})))
  }

  useEffect(() => {
    if (store.getState().coins.all.length === 0) {
      dispatch(initCoins({ coins: coins }))
      dispatch(setCoinsForView({ coins: coins }))
    }

    fetchAllData()

    if (interval.current === 0) {
      interval.current = window.setInterval(fetchAllData, 10000)
    }

    return () => {
      if (interval.current !== 0) {
        window.clearInterval(interval.current)
        interval.current = 0
      }
    }
  })
}
