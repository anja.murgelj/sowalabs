import { OhlcType } from '../store/data-slice';
import { normalizeCloseValues } from '../helpers';

const getOhlcFixture = (closeValues: string[]): OhlcType[] =>
  closeValues.map((value: string) => {
    return {
      open: '1',
      close: value,
      high: '1',
      low: '1',
      volume: '1',
      timestamp: '1641714000'
    }
  })

describe('normalizeCloseValues', () => {
  it('should return normalized values if array of values is an input', () => {
    const originalValues: OhlcType[] =
      getOhlcFixture(['10', '20', '30', '40', '50'])

    expect(normalizeCloseValues(originalValues)).toEqual([
      {value: 0}, {value: 0.25}, {value: 0.5}, {value: 0.75}, {value: 1}
    ])
  })

  it('should return empty array if input is empty array', () => {
    expect(normalizeCloseValues([])).toEqual([])
  })
})