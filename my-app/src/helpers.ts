import { CoinDescription, OhlcType } from './interfaces';

export const coins: CoinDescription[] = [
  {name: 'Bitcoin', abbr: 'BTC', value: 'btceur', description: 'Bitcoin BTC'},
  {name: 'Litecoin', abbr: 'LTC', value: 'ltceur', description: 'LiteCoin LTC'},
  {name: 'Etherium', abbr: 'ETH', value: 'etheur', description: 'Etherium ETH'},
  {name: 'Uniswap', abbr: 'UNI', value: 'unieur', description: 'Uniswap UNI'},
  {name: 'Ripple', abbr: 'XRP', value: 'xrpeur', description: 'Ripple XRP'},
  {name: 'Algorand', abbr: 'ALGO', value: 'algoeur', description: 'Algorand ALGO'},
  {name: 'Chainlink', abbr: 'LINK', value: 'linkeur', description: 'Chainlink LINK'},
  {name: 'Cardano', abbr: 'ADA', value: 'adaeur', description: 'Cardano ADA'},
  {name: 'The Graph', abbr: 'GRT', value: 'grteur', description: 'The Graph GRT'},
]

export const normalizeCloseValues = (data: OhlcType[]): { value: number }[] => {
  const values: number[] = data.map((data: OhlcType) => parseFloat(data.close))
  let min: number = Math.min(...values)
  let max: number = Math.max(...values)

  return values.map((v: number) => { return { value: ((v - min) / (max - min)) }})
}
